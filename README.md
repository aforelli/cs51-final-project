**Overview**

For our final CS51 project, we implemented an artificial intelligence solver to solve the popular 2048 game. This solver relies on a depth-first minimax search algorithm with alpha-beta testing to determine what direction the AI should move the board in.

The concept of minimax search is to evaluate possible future states of the board, assuming that the AI will play the move that maximizes its own score, and that the opponent (the computer, in this case) will play the worst possible move for the player. Based on these assumptions, the algorithm looks a certain number of steps into the future and sees what board each of the possible moves for the player would result in. The algorithm then returns the move that would result in the best possible grid for the player. 

In order to score the desirability of future states of the board, we created a function that evaluates how desirable a particular board is for the player according to three scoring heuristics. We chose these heuristics based on our own experience playing 2048.

Read more about the algorithm in the report section of the repository!