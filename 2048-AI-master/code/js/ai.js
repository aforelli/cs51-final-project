var max_depth = 6;
var branching_factor = 5;
var grid_height = 4;
var grid_width = 4;
var infinity = 10000;

function AI(grid) {
  this.grid = grid;
}

function evaluate (array_of_grid) {
  //weight given to different heuristics
  var emptySpacesWeight = .5;
  var mergeabilityWeight = .4;
  var cascadeWeight = .3;
  var maxTileWeight = .3;

  // initiating heuristics
  var emptySpaces= 0;
  var mergeability = 0;
  var cascade = 0;

  
  for (var i = 0; i < grid_height - 1; i++) {
    for (var j = 0; j < grid_width - 1; j++) {
      //empty spaces: grid will receive a better score if it has more empty cells;
      if (array_of_grid[j][i] == 0) {
      	emptySpaces++;
      }
      
      // cascade heuristic: grid will receive a better score if tiles are arranged in a
      // decreasing order along the grid. balanced against mergeability heuristic
      if (array_of_grid[i][j] < array_of_grid[i][j+1] || array_of_grid[i][j] == array_of_grid[i+1][j]) 
      {
          cascade++ ;
      }
    }
  }
  
      console.log ('after move in  \n' + array_toString(array_of_grid));
   var finalScore =  
    (emptySpaces * emptySpacesWeight + 
    array_mergeability(array_of_grid) * mergeabilityWeight + 
    cascade * cascadeWeight    +
    maxValue(array_of_grid) * maxTileWeight);
  return finalScore
}


// minimax with alpha-beta pruning
function search (depth, array, playerTurn, alpha, beta) {
  var score;
  if (depth == 0) {
    score = evaluate (array);
    return {score: score};
  }
  if (playerTurn) { 
    var bestScore = -infinity;
    // then move in all 4 directions for depth
    for (var direction = 0; direction < 4; direction ++) {
      var newArray = array_clone(array);
      array_move (newArray, direction);
      if (!array_same(array, newArray)) {  
         score = search(depth - 1, newArray, false, alpha, beta);
         bestScore = Math.max (bestScore, score.score);
         alpha = bestScore;
         //prunes any branch that alpha is higher than beta
         if (beta <= bestScore) {
           break;
         }
      }
    }
    return {score: bestScore, alpha: alpha} 
  }
  // computer's turn to spawn tiles - depends on the evaluate function
  else {
    var bestScore = infinity;
    var computerIter;
    var available = array_availableCells (array);
    // limits the number of branching factor to allow for deeper search
    if (available.length < branching_factor) {
      computerIter = available.length;
    }
    else {
      computerIter = branching_factor;
    }
    for (var i = 0; i < computerIter ; i++) {    
      var newArray = array_clone(array);    
      array_addRandomTile(newArray);
      score = search(depth - 1, newArray, true, alpha, beta);
      bestScore = Math.min (bestScore, score.score);
      beta = bestScore;
      // pruning
      if (bestScore <= alpha) {
        break;
      }
  }
  return {score: score.score, beta: beta} 
  }
}

function getBest (array) {
  var bestScore = -infinity;
  var bestMove = -1;
  var best = [];
  console.log ('before \n' + array_toString(array));
  for (var direction = 0; direction < 4; direction ++) {
    var newArray = array_clone(array);
    array_move (newArray, direction);
    if (!array_same (array, newArray)) { 
      var currentScore = search(max_depth, newArray, true, -infinity, infinity);
      best.push({score: currentScore.score, move: direction});
    }
  }
  for (var i = 0; i<best.length; i++) {
    if (best[i].score > bestScore) {
      bestScore = best[i].score;
      bestMove = best[i].move;
    }
  }
  return {move: bestMove}
}
