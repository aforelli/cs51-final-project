// CS51 Final Project: 2048 Solver using minimax
// Nhi Ho, Ali Forelli, Sarah Fellay
// May 2014
// this file handles all functions that handles arrays

// converts array to string for console.logging
function array_toString (array) {
  string = '';
  for (var j=0; j<4; j++) {
    for (var i=0; i<4; i++) {
        string += array[j][i] + ' ';
    }
    string += '\n';
  }
  return string
}

// checks for available cells in an array and return where these are
function array_availableCells (array) {
  var cells = [];
  for (var i=0; i<4; i++) {
    for (var j=0; j<4; j++) {
      if (array[i][j] == 0) {
        cells.push ({j: j, i: i});
      }
    }
  }
  return cells;
}

// deep clone of array
function array_clone (array) {
  var newArray = [];
  for (var i=0; i<array.length; i++) {
    newArray[i] = array[i].slice();
  }
  return newArray;
}

// find the first available random position of an array
function array_randomAvailableCells (array) {
  var cells = array_availableCells (array);
  if (cells.length) {
    //console.log ('random cell ' + cells[Math.floor(Math.random() * cells.length)]);
    return cells[Math.floor((Math.random() * cells.length) + 0)];
  }
}

// adds random tile
function array_addRandomTile (array) {
  var available = array_availableCells (array);
  if (array_availableCells (array)) {
    var value = Math.random() < 0.9 ? 2: 4;
    var tile = array_randomAvailableCells (array);
    array[tile.i][tile.j] = value;
  }
}

// tests if two arrays are the same
function array_same (array1, array2) {
 var moved = true; 
  for (var i=0; i<4; i++) {
    for (var j=0; j<4; j++) {
      if (array1[i][j] != array2[i][j]) {
        moved = false;
      }
    }
  }
  return moved
}

// helper function for array move
function unload_non_zeroes (gridArray, i, j, non_zeroes) {
  var current = non_zeroes.pop();
  //console.log ("current is " + current);
  if (typeof current != 'undefined') {
    var next = non_zeroes.pop();
    if (typeof next != 'undefined') {
      if (current == next) {
        gridArray[i][j] = 2 * current;
      }
      else {
        non_zeroes.push(next);
        gridArray[i][j] = current;
      }
    }
    else {
      gridArray[i][j] = current;
    }
  }
  else {
    gridArray[i][j] = 0;
  }
}

// moves an array
function array_move (oldArray, direction) {
  var non_zeroes = [];
  var gridArray = oldArray;
  switch(direction) {
    // move up
    case 0:
      for (var j = 0; j<4; j++) {
        for (var i=3; i>=0; i--) {
          if (gridArray[i][j] != 0) {
            non_zeroes.push(gridArray[i][j]);
          }
        }    
        for (var i = 0; i < 4; i++) {
          unload_non_zeroes (gridArray, i, j, non_zeroes);
        }
      }
      break;
    // move right
    case 1:
      for (var i=0; i<4; i++) {
        for (var j=0; j<4; j++) {
          if (gridArray[i][j] != 0) {
            non_zeroes.push(gridArray[i][j]);
          }
        }
        for (var j = 3; j >= 0; j--) {
          unload_non_zeroes (gridArray, i, j, non_zeroes);
        }
      }  
      break;
    // move down
    case 2:
      for (var j = 0; j<4; j++) {
        for (var i=0; i<4; i++) {
          if (gridArray[i][j] != 0) {          
            non_zeroes.push(gridArray[i][j]);
          }
        }
        //console.log ('non_zeroes ' + non_zeroes);
        for (var i = 3; i >= 0; i--) {
          unload_non_zeroes (gridArray, i, j, non_zeroes);
        }
      }
      break;
    // move left
    default:
      for (var i = 0; i<4; i++) {
        for (var j=3; j>=0; j--) {
          if (gridArray[i][j] != 0) {
            non_zeroes.push(gridArray[i][j]);
          }
        }  
        for (var j = 0; j < 4; j++) {
          unload_non_zeroes (gridArray, i, j, non_zeroes);
        }
      }
  }
  return gridArray
}


// the following are heuristic functions to access the board

// checks if things are in increasing or decreasing order
function monotonicity(array) {
  // scores for all four directions
  var totals = [0, 0];

  // up/down direction
  for (var x=0; x<4; x++) {
    var current = 0;
    var next = current+1;
    while ( next<4 ) {
      while ( next<4 && (array[x][next] == 0) ) {
        next++;
      }
      if (next>=4) { next--; }
      if (array[x][current] !== 0) {
        var currentValue = Math.log(array[x][current])/Math.log(2);
      }
      else { 
        var currentValue = 0;
      }
      if (array[x][next] !== 0) {
        var nextValue = Math.log(array[x][next])/Math.log(2);
      }
      else {
        var nextValue = 0;
      }
      var nextValue = array[x][next];
      if (currentValue >= nextValue) {
        //totals[0] += currentValue - nextValue;
        totals[0] += 1;
      } else if (nextValue >= currentValue) {
        //totals[2] += nextValue - currentValue;
        totals[0] += -1;
      }
      current = next;
      next++;
    }
  }

  // left/right direction
  for (var y=0; y<4; y++) {
    var current = 0;
    var next = current+1;
    while ( next<4 ) {
      while ( next<4 && (array[next][y] == 0)) {
        next++;
      }
      if (next>=4) { next--; }
      if (array[current][y] !== 0) {
        var currentValue = Math.log(array[current][y])/Math.log(2);
      }
      else { 
        var currentValue = 0;
      }
      if (array[next][y] !== 0) {
        var nextValue = Math.log(array[next][y])/Math.log(2);
      }
      else {
        var nextValue = 0;
      }
      if (currentValue >= nextValue) {
        //totals[3] += currentValue - nextValue;
        totals[1] += 1;
      } else if (nextValue >= currentValue) {
        //totals[1] += nextValue - currentValue;
        totals[1] += 1;
      }
      current = next;
      next++;
    }
  }

  return Math.abs(totals[0]) + Math.abs(totals[1]);
}

//checks if tiles of the same values are next to each other
function array_mergeability (array) {
  var mergeability = 0;
  for (var i=0; i<3; i++) {
    for (var j=0; j<3; j++) {
      if (array[i][j] == array[i][j+1]) {
        mergeability++;
      }
      if (array[i][j] == array[i+1][j]) {
        mergeability++;
      }
      // checks the last column
      if (j == 2) {
        if (array[i][3] == array[i+1][3]) {
          mergeability++;
        }
      }
    }
    // checks the bottom row
    if (i == 2) {
      if (array[3][i] == array[3][i+1]) {
        mergeability++;
      }
    }
  }
  return mergeability
}

function maxValue (array) {
  var maxTile = 0;
  var maxTileScore = 0;
  for (var i=0; i<4; i++){
   for (var j=0; j<4; j++) {
      if (array[j][i] > maxTile) {
       maxTile = array[j][i];
       maxTileScore = Math.log(maxTile)/Math.log(2);
      }
    }
  }
  // tried to keep max in corner, but took it out
  /*if (array[0][0] == maxTile || 
    array[0][3] == maxTile || 
    array[3][0] == maxTile || 
    array[3][3] == maxTile) {
    maxTileScore = 10;
  }*/
  return maxTileScore
}

function array_sum (array) {
  var sum = 0;
  for (var i=0; i<4; i++){
   for (var j=0; j<4; j++) {
      if (array[j][i] != 0) {
       sum += array[j][i];
      }
    }
  }
  return sum;
}

